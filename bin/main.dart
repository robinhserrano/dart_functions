num add(num x, num y) {
    return x + y;
}

Map<String, num> switchNumber(num num1, num2) {
    //No new variables are allowed
    return {
         "num1": num2,
         "num2": num1,
    };
}

int? countLetter(String letter, String sentence) {
    return letter == 'o' ? letter.allMatches(sentence).length : null;
}

bool isPalindrome(String text) {
    String txtOriginal = text.toLowerCase().split(' ').join('');
    String txtReversed = txtOriginal.split('').reversed.join();
    bool isPalindrome = txtOriginal == txtReversed;
    return isPalindrome;
}

bool isIsogram(String text) {
    var isIsogram = (text.toLowerCase().split(""))..remove("-")..remove(" ");
    return isIsogram.toSet().length == isIsogram.length;
}

num? purchase(int age, num price) {
    if(age < 13){
        return null;
    } else if(age >= 13 && age <=21 || age >= 60){
        return num.parse((price * 0.8).toStringAsFixed(2));
    } else if(age >= 22 && age <=64){ 
        return num.parse(price.toStringAsFixed(2));
    }
}

List<String> findHotCategories(List items) {
    List<String> hotCategories = [];
    items.forEach((item) {
       if(item['stocks'] == 0){
           hotCategories.add(item['category']);
       }
    });
    
    return hotCategories.toSet().toList();
}

List<String> findFlyingVoters(List candidateA, List candidateB) {
    candidateA.removeWhere((item) => !candidateB.contains(item));
    return candidateA as List<String>;
}

List<Map<String, dynamic>> calculateAdEfficiency(List items) {
    List<Map<String, dynamic>> brandEfficiency = [];

    items.forEach((item) {
       brandEfficiency.add({
           'brand' : item['brand'],
           'adEfficiency' : item['customersGained']/item['expenditure'] * 100
       }); 
    });
    
    brandEfficiency.sort((a, b) => b["adEfficiency"].compareTo(a["adEfficiency"]));
    
    return brandEfficiency;
}
